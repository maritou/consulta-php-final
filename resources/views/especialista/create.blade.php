@extends('web.common.base')

@section('content')

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            @if(session('status'))
            <div class="alert alert-success">
                {{session('status')}}
            </div>
            @endif
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Crear Especialista</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-wrench"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="/examen">Volver</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="ibox-content">
                    <form id="form-especialista-new" class="form-horizontal" role="form" method="post" action="{{ action('EspecialistaController@create') }}">
                        {{ csrf_field() }}
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="nombres">Nombre</label>
                                <input type="text" class="form-control" name='nombres'id="nombres" placeholder="Nombre">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="apellidos">Apellidos</label>
                                <input type="text" class="form-control" name='apellidos' id="apellidos" placeholder="Apellidos">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="direccion">Direccion</label>
                                <input type="text" class="form-control" name='direccion' id="direccion" placeholder="Direccion">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="telefono">Telefono</label>
                                <input type="text" class="form-control" name='telefono' id="telefono" placeholder="Telefono">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="email">Email</label>
                                <input type="email" class="form-control" name='email' id="email" placeholder="Email">
                            </div>
                        </div>
                        <div class="float-right">
                            <button type="submit" class="btn btn-primary">Crear Especialistas</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>












@endsection()