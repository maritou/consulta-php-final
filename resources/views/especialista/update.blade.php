@extends('web.common.base')

@section('content')

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            @if(session('status'))
            <div class="alert alert-success">
                {{session('status')}}
            </div>
            @endif
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Actualizar Especialista</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-wrench"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="/examen">Volver</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="ibox-content">
                    <form id="form-especialista-new" class="form-horizontal" role="form" method="post" action="{{ action('EspecialistaController@update') }}">
                        {{ csrf_field() }}
                        <input type="hidden" name="id" id="id-u" value="{{ $especialista->id }}">
                        <input type="hidden" name="type" value="update">
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="nombres">Nombre</label>
                                <input type="text" class="form-control" name='nombres' id="nombres" value="{{$especialista->nombres}}">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="apellidos">Apellidos</label>
                                <input type="text" class="form-control" name='apellidos' id="apellidos" value="{{$especialista->apellidos}}">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="direccion">Direccion</label>
                                <input type="text" class="form-control" name='direccion' id="direccion" value="{{$especialista->direccion}}">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="telefono">Telefono</label>
                                <input type="text" class="form-control" name='telefono' id="telefono" value="{{$especialista->telefono}}">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="email">Email</label>
                                <input type="email" class="form-control" name='email' id="email" value="{{$especialista->email}}">
                            </div>
                        </div>
                        <div class="float-right">
                            <button type="submit" class="btn btn-primary">Actualizar Especialista</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>












@endsection()