@extends('web.common.base')

@section('content')

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            @if(session('status'))
            <div class="alert alert-success">
                {{session('status')}}
            </div>
            @endif
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Crear Especialidad</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-wrench"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="/examen">Volver</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="ibox-content">
                    <form id="form-especialidad-new" class="form-horizontal" role="form" method="post" action="{{ action('EspecialidadController@create') }}">
                        {{ csrf_field() }}
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="descripcion">Descripcion</label>
                                <input type="text" class="form-control" name='descripcion' id="descripcion" placeholder="Descripcion">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="SelectEspecialidad">Disponibilidad</label>
                                <select class="form-control" name="enable" id="SelectEspecialidad">
                                    <option value="0">Seleccione una opcion</option>
                                    <option value="Disponible">Disponible</option>
                                    <option value="No disponible">No disponible</option>
                                </select>
                            </div>
                        </div>
                        <div class="float-right">
                            <button type="submit" class="btn btn-primary">Crear Especialidad</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>












@endsection()