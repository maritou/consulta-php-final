@extends('web.common.base')

@section('content')

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            @if(session('status'))
            <div class="alert alert-success">
                {{session('status')}}
            </div>
            @endif
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Crear Examen</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-wrench"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="/examen">Volver</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="ibox-content">
                    <form id="form-examen-new" class="form-horizontal" role="form" method="post" action="{{ action('ExamenController@create') }}">
                        {{ csrf_field() }}
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="descripcion">Descripcion</label>
                                <input type="text" class="form-control" name='descripcion'id="descripcion" placeholder="Descripcion">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="preparacion">Preparacion</label>
                                <input type="text" class="form-control" name='preparacion' id="preparacion" placeholder="Preparacion">
                            </div>
                        </div>
                        <div class="float-right">
                            <button type="submit" class="btn btn-primary">Crear Examen</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>












@endsection()