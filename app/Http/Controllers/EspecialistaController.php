<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Especialista;

class EspecialistaController extends Controller
{
    /**
     * LIST DATA especialista
     */
    public function index()
    {
        $data = array();
        $data['especialista'] = Especialista::all();

        return view('especialista.especialista', $data);
    }
    /**
     * CREATE NEW especialista VIEW
     * @return type
     */
    public function createView()
    {

        return view('especialista.create');
    }

    /**
     * CREATE SAVE especialista
     * @param \App\Http\Controllers\Request $request
     */
    public function create(Request $request)
    {


        $especialista = new Especialista();
        $especialista->nombres = $request->nombres;
        $especialista->apellidos = $request->apellidos;
        $especialista->direccion = $request->direccion;
        $especialista->telefono = $request->telefono;
        $especialista->email = $request->email;

        $especialista->save();

        return redirect('especialista');
    }

    /**
     * SHOW DATA FOR UPDATE especialista
     * @param type $id
     */
    public function updateView($id)
    {
        $data = array();

        $data['especialista'] = Especialista::where('id', '=', $id)
            ->first();

        return view('especialista.update', $data);
    }
    /**
     * UPDATE SAVE especialista
     * @param Request $request
     */
    public function update(Request $request)
    {
        $especialista = Especialista::find($request->id);
        $especialista->nombres = $request->nombres;
        $especialista->apellidos = $request->apellidos;
        $especialista->direccion = $request->direccion;
        $especialista->telefono = $request->telefono;
        $especialista->email = $request->email;
        $especialista->save();

        return redirect('especialista')->with('status', 'Especialista Actualizado Correctamente');
    }
    /*
     * DELETE especialista
     * @param type $id
     */

    public function delete($id)
    {
        $especialista = Especialista::find($id);
        $especialista->delete($id);
        $mensaje = "Especialista Eliminado";

        return redirect('especialista')->with('status', $mensaje);
    }
}
