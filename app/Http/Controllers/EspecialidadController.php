<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Especialidad;

class EspecialidadController extends Controller
{
    /**
     * LIST DATA especialidad
     */
    public function index()
    {
        $data = array();
        $data['especialidad'] = Especialidad::all();

        return view('especialidad.especialidad', $data);
    }
    /**
     * CREATE NEW especialidad VIEW
     * @return type
     */
    public function createView()
    {

        return view('especialidad.create');
    }

    /**
     * CREATE SAVE especialidad
     * @param \App\Http\Controllers\Request $request
     */
    public function create(Request $request)
    {


        $especialidad = new Especialidad();
        $especialidad->descripcion = $request->descripcion;
        $especialidad->enable = $request->enable;
        $especialidad->save();

        return redirect('especialidad');
    }

    /**
     * SHOW DATA FOR UPDATE especialidad
     * @param type $id
     */
    public function updateView($id)
    {
        $data = array();

        $data['especialidad'] = Especialidad::where('id', '=', $id)
            ->first();

        return view('especialidad.update', $data);
    }
    /**
     * UPDATE SAVE especialidad
     * @param Request $request
     */
    public function update(Request $request)
    {
        $especialidad = Especialidad::find($request->id);
        $especialidad->descripcion = $request->descripcion;
        $especialidad->enable = $request->enable;

        $especialidad->save();

        return redirect('especialidad')->with('status', 'Especialidad Actualizado Correctamente');
    }
    /*
     * DELETE especialidad
     * @param type $id
     */

    public function delete($id)
    {
        $especialidad = Especialidad::find($id);
        $especialidad->delete($id);
        $mensaje = "Especialidad Eliminado";

        return redirect('especialidad')->with('status', $mensaje);
    }
}
