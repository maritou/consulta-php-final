<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Examen;

class ExamenController extends Controller
{
    /**
     * LIST DATA examens
     */
    public function index()
    {
        $data = array();
        $data['examens'] = Examen::all();

        return view('examen.examen', $data);
    }
    /**
     * CREATE NEW examens VIEW
     * @return type
     */
    public function createView()
    {

        return view('examen.create');
    }

    /**
     * CREATE SAVE examens
     * @param \App\Http\Controllers\Request $request
     */
    public function create(Request $request)
    {


        $examen = new Examen();
        $examen->descripcion = $request->descripcion;
        $examen->preparacion = $request->preparacion;

        $examen->save();

        return redirect('examen');
    }

    /**
     * SHOW DATA FOR UPDATE examens
     * @param type $id
     */
    public function updateView($id)
    {
        $data = array();

        $data['examen'] = Examen::where('id', '=', $id)
            ->select('id', 'descripcion', 'preparacion')
            ->first();

        return view('examen.update', $data);
    }
    /**
     * UPDATE SAVE admin_users
     * @param Request $request
     */
    public function update(Request $request)
    {
        $examen = Examen::find($request->id);
        $examen->descripcion = $request->descripcion;
        $examen->preparacion = $request->preparacion;
        $examen->save();

        return redirect('examen')->with('status', 'Examen Actualizado Correctamente');
    }
    /*
     * DELETE examen
     * @param type $id
     */

    public function delete($id)
    {
        $examen = Examen::find($id);
        $examen->delete($id);
        $mensaje = "Examen Eliminado";

        return redirect('examen')->with('status', $mensaje);
    }
}
