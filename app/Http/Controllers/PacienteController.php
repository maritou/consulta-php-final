<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Paciente;

class PacienteController extends Controller
{
    /**
     * LIST DATA paciente
     */
    public function index()
    {
        $data = array();
        $data['paciente'] = Paciente::all();

        return view('paciente.paciente', $data);
    }
    /**
     * CREATE NEW paciente VIEW
     * @return type
     */
    public function createView()
    {

        return view('paciente.create');
    }

    /**
     * CREATE SAVE paciente
     * @param \App\Http\Controllers\Request $request
     */
    public function create(Request $request)
    {


        $paciente = new Paciente();
        $paciente->nombres = $request->nombres;
        $paciente->apellidos = $request->apellidos;
        $paciente->direccion = $request->direccion;
        $paciente->telefono = $request->telefono;
        $paciente->email = $request->email;

        $paciente->save();

        return redirect('paciente');
    }

    /**
     * SHOW DATA FOR UPDATE paciente
     * @param type $id
     */
    public function updateView($id)
    {
        $data = array();

        $data['paciente'] = Paciente::where('id', '=', $id)
            ->first();

        return view('paciente.update', $data);
    }
    /**
     * UPDATE SAVE paciente
     * @param Request $request
     */
    public function update(Request $request)
    {
        $paciente = Paciente::find($request->id);
        $paciente->nombres = $request->nombres;
        $paciente->apellidos = $request->apellidos;
        $paciente->direccion = $request->direccion;
        $paciente->telefono = $request->telefono;
        $paciente->email = $request->email;
        $paciente->save();

        return redirect('paciente')->with('status', 'Paciente Actualizado Correctamente');
    }
    /*
     * DELETE paciente
     * @param type $id
     */

    public function delete($id)
    {
        $paciente = Paciente::find($id);
        $paciente->delete($id);
        $mensaje = "Paciente Eliminado";

        return redirect('paciente')->with('status', $mensaje);
    }
}

