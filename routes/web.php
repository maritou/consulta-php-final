<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* RUTAS EXAMEN */ 
Route::get('/examen', 'ExamenController@index');
Route::get('/examen/new', 'ExamenController@createView');
Route::post('/examen/new', 'ExamenController@create');
Route::get('/examen/update/{id}', 'ExamenController@updateView');
Route::post('/examen/update', 'ExamenController@update');
Route::get('/examen/delete/{id}', 'ExamenController@delete');

/* RUTAS ESPECIALISTA */ 
Route::get('/especialista', 'EspecialistaController@index');
Route::get('/especialista/new', 'EspecialistaController@createView');
Route::post('/especialista/new', 'EspecialistaController@create');
Route::get('/especialista/update/{id}', 'EspecialistaController@updateView');
Route::post('/especialista/update', 'EspecialistaController@update');
Route::get('/especialista/delete/{id}', 'EspecialistaController@delete');

/* RUTAS PACIENTE */ 
Route::get('/paciente', 'PacienteController@index');
Route::get('/paciente/new', 'PacienteController@createView');
Route::post('/paciente/new', 'PacienteController@create');
Route::get('/paciente/update/{id}', 'PacienteController@updateView');
Route::post('/paciente/update', 'PacienteController@update');
Route::get('/paciente/delete/{id}', 'PacienteController@delete');

/* RUTAS ESPECIALIDAD */ 
Route::get('/especialidad', 'EspecialidadController@index');
Route::get('/especialidad/new', 'EspecialidadController@createView');
Route::post('/especialidad/new', 'EspecialidadController@create');
Route::get('/especialidad/update/{id}', 'EspecialidadController@updateView');
Route::post('/especialidad/update', 'EspecialidadController@update');
Route::get('/especialidad/delete/{id}', 'EspecialidadController@delete');

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');
Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('register', 'Auth\RegisterController@register');
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('password.update');